# spring-boot-user-account

This project explains Save, Get user using spring boot and H2 in-memory database.
In this app we are using Spring Data JPA for built-in methods to do user management operations.     
`@EnableJpaRepositories` annotation is used on main class to Enable H2 DB related configuration, which will read properties from `application.properties` file.
## Prerequisites 
- Java
- Spring Boot
- Maven
- H2 Database
- Lombok
- JUnit


## Tools
- Eclipse
- Maven (version >= 3.6.0)
- Postman


###  Build and Run application
_GOTO >_ **~/absolute-path-to-directory/spring-boot-user-account**  
and try below command in terminal
> **```mvn spring-boot:run```** it will run application as spring boot application

or
> **```mvn clean install```** it will build application and create **jar** file under target directory 

Run jar file from below path with given command
> **```java -jar ~/path-to-spring-boot-h2-crud/target/spring-boot-h2-crud-0.0.1-SNAPSHOT.jar```**

Or
> run main method from `SpringBootUserApplication.java` as spring boot application.  


||
|  ---------    |
| **_Note_** : In `SpringBootUserApplication.java` class we have autowired User repositorie. If there is no record present in DB , static data is getting inserted in DB from `HelperUtil.java` class when we are starting the app for the first time.| 
### Code Snippets
1. #### Maven Dependencies
    Need to add below dependencies to enable H2 DB related config in **pom.xml**. Lombok's dependency is to get rid of boiler-plate code.   
    ```
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-data-jpa</artifactId>
    </dependency>
   
    <!-- update -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-aop</artifactId>
    </dependency>
   
    <dependency>
        <groupId>com.h2database</groupId>
        <artifactId>h2</artifactId>
        <scope>runtime</scope>
    </dependency>
   
    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <optional>true</optional>
    </dependency>
    ```
   
   Added Spring Boot DevTools to pick up the changes and restart the application.
    ```
   <dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>
			<scope>runtime</scope>
	</dependency>
    ```
    Added Junit to generate the unit test and integration test.
    ```
   <dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter</artifactId>
			<version>5.6.0</version>
			<scope>test</scope>
	</dependency>
	<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-engine</artifactId>
			<version>5.6.0</version>
			<scope>test</scope>
	</dependency>
	<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-params</artifactId>
			<version>5.6.0</version>
			<scope>test</scope>
	</dependency>
    ```
   
   
2. #### Properties file
    Reading H2 DB related properties from **application.properties** file and configuring JPA connection factory for H2 database.  

    **src/main/resources/application.properties**
     ```
        server.port=8088

        spring.datasource.url=jdbc:h2:mem:sampledb
        spring.datasource.driverClassName=org.h2.Driver
        spring.datasource.username=sa
        spring.datasource.password=password
        spring.jpa.database-platform=org.hibernate.dialect.H2Dialect

        spring.h2.console.enabled=true

        spring.jpa.properties.hibernate.show_sql=true
        spring.jpa.properties.hibernate.use_sql_comments=true
        spring.jpa.properties.hibernate.format_sql=true

        springdoc.version=1.0.0
        springdoc.swagger-ui.path=/swagger-ui-custom.html
     ```
   
   
3. #### Model class 
    **User.java**  
    ```
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @Entity
    @Table(name = "Utilisateurs")
    public class User implements Serializable {
    	private static final long serialVersionUID = 1L;
    	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
    	private Long id;
    	@NotNull
    	private String name;
    	@NotNull
    	@JsonFormat(pattern = "dd/MM/yyyy")
    	private LocalDate birthday;
    	@NotNull
    	@Enumerated(EnumType.STRING)
    	private Country countryResidence;
    	private String phoneNumber;
    	@Enumerated(EnumType.STRING)
    	private Gender gender;
    }
    ```
   
    #### **Update**: New annotations are added in this application to print log smartly with the help of Aspect (LoggerAspect class from aop package).
    **@LogObjectBefore** - annotation created to print log before method execution 
    **@LogObjectAfter** - annotation created to print the returned value from method
    #   
   
   
4. #### API Endpoints 
     In **DefaultController.java** class, 
    we have exposed 2 endpoints 
    - GET redirect to swagger-ui-custom.html
    - GET redirect to db
    ```
    @RestController
    public class DefaultController {
        @Operation(summary = "Swagger documentation url", hidden = true)
        @GetMapping
        ResponseEntity<Void> redirect() {
            return ResponseEntity.status(HttpStatus.FOUND)
                    .location(URI.create("swagger-ui-custom.html"))
                    .build();
        }
    
        @Operation(summary = "Database url", hidden = true)
        @GetMapping("/db")
        public ResponseEntity<Void> databaseUrl() {
            return ResponseEntity.status(HttpStatus.FOUND)
                    .location(URI.create("h2-console"))
                    .build();
        }
    }
    ```
    
    In **UserController.java** class, 
    we have exposed 2 endpoints 
    - GET User by ID
    - POST to store User
    ```
    @RestController
    @RequestMapping("/user")
    public class UserController {
    
    	@Autowired
    	private UserService userService;

    	@LogObjectAfter
    	@GetMapping("/{id}")
    	public ResponseEntity<?> findById(@PathVariable long id) {
    		UserDTO user = userService.getOneUserById(id);
    		return ResponseEntity.ok().body(user);
    	}
    	
    	@LogObjectBefore
    	@LogObjectAfter
    	@PostMapping
    	public ResponseEntity<?> save(@Valid @RequestBody UserDTO userDTO) {
    		UserDTO savedUser = userService.saveUser(userDTO);
    
    		URI uri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/{id}").buildAndExpand(savedUser.getId())
    				.toUri();
    		return ResponseEntity.created(uri).body(savedUser);
    	}
    }
    ```
     ```
        > **GET Mapping** http://localhost:8088  - redirect to swagger-ui
    
        > **GET Mapping** http://localhost:8088/db  - redirect to H2-console
        
        > **GET Mapping** http://localhost:8088/user/1  - Get User by ID
           
        > **POST Mapping** http://localhost:8088/user  - Add new User in DB  
     ```
    ### Request Body  
    ```
        {
              "name": "aymen",
              "birthday": "29/12/1990",
              "countryResidence": "FRENCH",
              "phoneNumber": "0700000000",
              "gender": "M"
        }
    ```
    
4. #### Usage
    Run the project through the IDE and head out to http://localhost:8088
    or
    To try it you can use Postman Collection :
    resources->postman collection->USER-ACCOUNT.postman_collection.json
    
       
   

