package com.spring.useraccount.demo.enumeration;

import java.util.Arrays;
import java.util.Optional;
/**
 * Class for Enumerate the Gender.
 * 
 * @author Azem Aymen
 */
public enum EnumGender {
    M("MALE"),
    F("FEMALE"),
    A("ATHORS");

    private String name;

    public String getGender() {
        return name;
    }

    EnumGender(String gender) {
        this.name = gender;
    }

    public static Optional<EnumGender> getGenderFromString(String value) {
        return Arrays.stream(EnumGender.values())
                .filter(gender -> gender.name.equalsIgnoreCase(value))
                .findFirst();

    }
}
