package com.spring.useraccount.demo.service.impl;

import java.time.LocalDate;

import org.springframework.stereotype.Service;

import com.spring.useraccount.demo.dto.DTOBuilder;
import com.spring.useraccount.demo.dto.UserDTO;
import com.spring.useraccount.demo.enumeration.EnumCountry;
import com.spring.useraccount.demo.errors.IllegalBusinessLogiqueException;
import com.spring.useraccount.demo.errors.ResourceNotFoundException;
import com.spring.useraccount.demo.model.User;
import com.spring.useraccount.demo.repository.UserRepository;
import com.spring.useraccount.demo.service.UserService;

/**
 * Implementation of the interface UserService .
 * 
 * @author Azem Aymen
 */
@Service
public class UserServiceImpl implements UserService {

	private final UserRepository userRepository;

	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	/**
	 * Save and return the user.
	 * 
	 * @param UserDTO
	 */
	@Override
	public UserDTO saveUser(UserDTO userDTO) {
		if (!checkIsAdultFrensh(userDTO))
			throw new IllegalBusinessLogiqueException("Only adult French residents are allowed to create an account!");
		User user = DTOBuilder.toUser(userDTO);
		return DTOBuilder.toUserDTO(userRepository.save(user));
	}

	/**
	 * Return the user account by id.
	 * 
	 * @param id of user
	 */
	@Override
	public UserDTO getUserById(Long id) {
		User user = userRepository.findById(id).orElse(null);
		if (user == null)
			throw new ResourceNotFoundException("User not found with id = " + id);
		return DTOBuilder.toUserDTO(user);
	}

	private boolean checkIsAdultFrensh(UserDTO userDTO) {
		return userDTO.getCountryResidence().equals(EnumCountry.FRENCH)
				&& (LocalDate.now().getYear() - userDTO.getBirthDate().getYear() >= 18);
	}

}
