package com.spring.useraccount.demo.dto;

import com.spring.useraccount.demo.model.User;

/**
 * Class for mapping between entity and DTO.
 * 
 * @author Azem Aymen
 */
public class DTOBuilder {

	private DTOBuilder() {
	}

	/**
	 * Mapped UserDTO to User.
	 * 
	 * @param UserDTO
	 */
	public static User toUser(UserDTO userDTO) {
		return User.builder().name(userDTO.getName()).birthDate(userDTO.getBirthDate())
				.countryResidence(userDTO.getCountryResidence()).gender(userDTO.getGender())
				.phoneNumber(userDTO.getPhoneNumber()).build();
	}

	/**
	 * Mapped User to UserDTO.
	 * 
	 * @param User
	 */
	public static UserDTO toUserDTO(User user) {
		return UserDTO.builder().id(user.getId()).name(user.getName()).birthDate(user.getBirthDate())
				.countryResidence(user.getCountryResidence()).gender(user.getGender())
				.phoneNumber(user.getPhoneNumber()).build();
	}

}
