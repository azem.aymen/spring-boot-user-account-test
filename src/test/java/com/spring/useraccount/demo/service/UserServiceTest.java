package com.spring.useraccount.demo.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.spring.useraccount.demo.dto.DTOBuilder;
import com.spring.useraccount.demo.dto.UserDTO;
import com.spring.useraccount.demo.enumeration.EnumCountry;
import com.spring.useraccount.demo.errors.IllegalBusinessLogiqueException;
import com.spring.useraccount.demo.errors.ResourceNotFoundException;
import com.spring.useraccount.demo.model.User;
import com.spring.useraccount.demo.repository.UserRepository;
import com.spring.useraccount.demo.service.impl.UserServiceImpl;

/**
 * @author Azem Aymen
 */
@ExtendWith(MockitoExtension.class)
class UserServiceTest {

	private UserServiceImpl userService;

	@Mock
	private UserRepository userRepository;

	private User user = User.builder().id(1L).name("Aymen").birthDate(LocalDate.of(1990, 12, 10))
			.countryResidence(EnumCountry.FRENCH).build();

	@BeforeEach
	void init() {
		userService = new UserServiceImpl(userRepository);
	}

	@Test
	@DisplayName("find user by Id successfully")
	void getOneUserByIdOK() {
		when(userRepository.findById(1L)).thenReturn(Optional.of(this.user));
		UserDTO returnedUser = userService.getUserById(1L);
		Assertions.assertSame(returnedUser.getName(), this.user.getName());
		Assertions.assertSame(returnedUser.getId(), this.user.getId());
	}

	@Test
	@DisplayName("find user by Id throw ResourceNotFoundException")
	void getOneUserByIdKO() {
		when(userRepository.findById(2L)).thenReturn(Optional.empty());
		ResourceNotFoundException thrown = assertThrows(ResourceNotFoundException.class,
				() -> userService.getUserById(2L), "User not found with id = 2");

		Assertions.assertThrows(ResourceNotFoundException.class, () -> userService.getUserById(2L));
		Assertions.assertEquals("User not found with id = 2", thrown.getMessage());
	}

	@Test
	@DisplayName("save new user successfully")
	void saveUserOK() {
		UserDTO mockuserDTO = UserDTO.builder().name("Aymen").birthDate(LocalDate.of(1990, 12, 29))
				.countryResidence(EnumCountry.FRENCH).build();
		User userrepo = DTOBuilder.toUser(mockuserDTO);
		when(userRepository.save(any())).thenReturn(userrepo);

		UserDTO saveUser = userService.saveUser(mockuserDTO);
		Assertions.assertSame(saveUser.getName(), userrepo.getName());
		Assertions.assertSame(saveUser.getCountryResidence(), userrepo.getCountryResidence());
	}

	@Test
	@DisplayName("save new user throw IllegalBusinessLogiqueException")
	void saveUserKO_IllegalBusinessLogiqueException() {
		UserDTO mockuserDTO = UserDTO.builder().name("Aymen").birthDate(LocalDate.of(2020, 12, 29))
				.countryResidence(EnumCountry.FRENCH).build();
		Assertions.assertThrows(IllegalBusinessLogiqueException.class, () -> userService.saveUser(mockuserDTO));
	}

}